import { Response, Request, NextFunction } from 'express'
import { OK, BAD_REQUEST, NOT_FOUND } from '../../constants/statusCodes'
import db from '../../database/models/'
import jsonResponse from '../../utils/jsonResponse'
import moment from 'moment'
import CRUDController from '../../utils/crud'
import { Op } from 'sequelize'

/**
 * handles the creation,updating
 * and getting all the invoices
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @const {uuidv4}uui
 * @returns {*} response
 */
class InvoiceHandlerController extends CRUDController {
  protected model: string = 'Invoice'
  /**
   * Check for an invoice in the system
   * @param {*} req
   * @param {*} res
   * @param {*} next
   * @const {uuidv4}uui
   * @returns {*} response
   */

  public checkRecord = async (req: Request, res: Response, next: NextFunction) => {
    const {
      params: { id },
      currentUser,
    }: any = req
    const record = await db[this.model].findOne({
      where: {
        id: { [Op.eq]: id },
        userId: {
          [Op.eq]: currentUser.id,
        },
      },
      include: [
        {
          model: db['Order'],
          as: 'order',
          include: [
            {
              model: db['OrderItem'],
              as: 'orderItems',
              include: [
                {
                  model: db['Product'],
                  as: 'product',
                  attributes: ['categoryId', 'productName', 'description', 'photos'],
                },
              ],
            },
          ],
        },
      ],
    })
    if (!record) {
      return jsonResponse({
        res,
        status: NOT_FOUND,
        message: `Record not found`,
      })
    }
    req['invoice'] = record
    return next()
  }

  /**
   *@param {*} req
   *@param {*} res
   *@returns {*} Response
   * @memberof InvoiceHandlerController
   */
  public getRecord = async (req: any, res: Response) => {
    const { invoice } = req
    return jsonResponse({ res, status: OK, invoice })
  }

  /**
   *@param {*} req
   *@param {*} res
   *@returns {*} Response
   * @memberof InvoiceHandlerController
   */
  public updateInvoice = async (req: Request, res: Response) => {
    const {
      body: { paymentStatus },
      params: { id },
      currentUser,
    }: any = req

    await db[this.model].update(
      { paymentStatus: paymentStatus },
      { where: { id: { [Op.eq]: id }, userId: { [Op.eq]: currentUser.id } } },
    )
    return jsonResponse({
      res,
      status: OK,
      message: 'your payment status has been update successfully',
    })
  }

  /**
   * @param {*} req
   * @param {*} res
   * @returns {*} response
   */
  public getMany = async (req: Request, res: Response) => {
    const { currentUser }: any = req
    const allInvoices = await db[this.model].findAndCountAll({
      where: {
        userId: currentUser.id,
      },
      include: [
        {
          model: db['Order'],
          as: 'order',
          include: [
            {
              model: db['OrderItem'],
              as: 'orderItems',
              include: [
                {
                  model: db['Product'],
                  as: 'product',
                  attributes: ['categoryId', 'productName', 'description', 'photos'],
                },
              ],
            },
          ],
        },
      ],
    })
    return jsonResponse({
      res,
      status: OK,
      invoices: allInvoices,
      meta: {
        total: allInvoices.count,
        page: 1,
        pages: 1,
      },
    })
  }
}

export default InvoiceHandlerController
