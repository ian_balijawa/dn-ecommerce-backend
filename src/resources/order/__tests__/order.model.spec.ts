import { expect } from 'chai'

const { sequelize, dataTypes, checkModelName, checkPropertyExists } = require('sequelize-test-helpers')
const { OrderItemInit } = require('../orderitem')
const { OrderInit } = require('../order.model')

describe('/models/OrderItem', () => {
  const Model = OrderItemInit(sequelize)
  const instance = new Model()
  const Order = OrderInit(sequelize)

  checkModelName(Model)('OrderItem')
  context('properties', () => {
    ;['id', 'orderId', 'userId', 'productId', 'quantity', 'amount'].forEach(checkPropertyExists(instance))
  })

  before(() => {
    Model.associate({ Order })
  })
  context('check associations', () => {
    it('defined a belongsTo association with Order', () => {
      expect(Model.belongsTo).to.have.been.calledWith(Order)
    })
  })
})
