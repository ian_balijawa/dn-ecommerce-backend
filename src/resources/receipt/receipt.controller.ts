import { Response, Request } from 'express'
import moment from 'moment'
import CRUDController from '../../utils/crud'
import db from '../../database/models'
import ReceiptService from '../../services/ReceiptService'
import jsonResponse from '../../utils/jsonResponse'
import { OK } from '../../constants/statusCodes'

/**
 * Provides methods to process receipts
 */
class ReceiptsController extends CRUDController {
  public template: string = 'default.hbs'
  public model: string = 'Invoice'
  protected subModelBelongsTo: string = 'Order'
  protected subModelBelongsToField: string = 'order'
  protected modelAssociations: any = {
    include: [
      {
        model: db[this.subModelBelongsTo],
        as: this.subModelBelongsToField,
        include: [
          {
            model: db['OrderItem'],
            as: 'orderItems',
            include: [
              {
                model: db['Product'],
                as: 'product',
                attributes: ['categoryId', 'productName', 'description', 'photos'],
              },
            ],
          },
        ],
      },
    ],
  }

  /**
   * Generate PDF receipts
   * @param {any} req
   * @param {any} res
   * @param {function} next
   * @returns {*} a Response
   */
  public downloadReceipt = async (req: any, res: any) => {
    const { [this.model.toLowerCase()]: invoice } = req
    const data = invoice.get({ plain: true })
    if (!data.billing) data.billing = {}

    const RECIEPT = new ReceiptService(this.template)
    await RECIEPT.compile(data)
    const pdf = await RECIEPT.generate()
    res.set({
      'Content-disposition': `attachment; filename=${moment.now()}.pdf`,
      'Content-Type': 'application/pdf',
      'Content-Length': pdf.length,
    })
    res.status(200)
    return res.send(pdf)
  }

  /**
   * Get many receipts
   * @param {any} req
   * @param {any} res
   * @returns {object} Response
   */
  public getMany = async (req: Request, res: Response) => {
    const { currentUser }: any = req
    let receipts = await db[this.model].findAll({
      where: {
        userId: currentUser.id,
        paymentStatus: 'paid',
      },
      ...this.modelAssociations,
    })

    return jsonResponse({
      res,
      status: OK,
      receipts,
      meta: {
        total: receipts.length,
        page: 1,
        pages: 1,
      },
    })
  }

  /**
   * Retrieve a single record
   * @param {*} req
   * @param {*} res
   * @returns {*} Response
   */
  public getOne = (req: any, res: any) => {
    return jsonResponse({
      res,
      status: OK,
      receipt: req[this.singleRecord()].get(),
    })
  }
}

export default new ReceiptsController()
