// test
module.exports = {
  '/receipts': {
    get: {
      tags: ['receipt'],
      summary: 'Retrieve all receipts',
      parameters: [{ in: 'header', name: 'Authorization', description: 'JWT Token', required: true, type: 'string' }],
      schema: {
        $ref: '#/definitions/Receipt',
      },
      responses: {
        '401': {
          description: 'Unauthorized access',
          example: {
            status: 401,
            message: 'Unauthorized Access',
          },
        },
        '403': {
          description: 'Forbidden access',
          example: {
            status: 403,
            message:
              'The invoice associated with this receipt is still pending, You will not be allowed access to a receipt',
          },
        },
      },
    },
  },
  '/receipts/{id}': {
    get: {
      tags: ['receipt'],
      summary: 'Retrieve a specific receipt that has been paid',
      parameters: [
        { in: 'header', name: 'Authorization', description: 'JWT Token', required: true, type: 'string' },
        {
          in: 'path',
          name: 'id',
          description: 'Receipt ID',
          required: true,
        },
      ],
      responses: {
        '200': {
          description: 'OK',
          example: {
            status: 200,
            order: '#/definitions/Receipt',
          },
        },
        '401': {
          description: 'Unauthorized access',
          example: {
            status: 401,
            message: 'Unauthorized access',
          },
        },
        '403': {
          description: 'Forbidden access',
          example: {
            status: 403,
            message:
              'The invoice associated with this receipt is still pending, You will not be allowed access to a receipt',
          },
        },
        '404': {
          description: 'Receipt not found',
          example: {
            status: 404,
            message: 'Receipts not foud',
          },
        },
      },
    },
  },
  '/receipts/download/{id}': {
    get: {
      tags: ['receipt'],
      summary: 'Retrieve a specific receipt that has been paid and download',
      parameters: [
        { in: 'header', name: 'Authorization', description: 'JWT Token', required: true, type: 'string' },
        {
          in: 'path',
          name: 'id',
          description: 'Receipt ID',
          required: true,
        },
      ],
      responses: {
        '200': {
          description: 'OK',
          example: {
            status: 200,
            order: '#/definitions/Receipt',
          },
        },
        '401': {
          description: 'Unauthorized access',
          example: {
            status: 401,
            message: 'Unauthorized access',
          },
        },
        '403': {
          description: 'Forbidden access',
          example: {
            status: 403,
            message:
              'The invoice associated with this receipt is still pending, You will not be allowed access to a receipt',
          },
        },
        '404': {
          description: 'Order not found',
          example: {
            status: 404,
            message: 'Orders not foud',
          },
        },
      },
    },
  },
}
